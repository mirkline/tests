import React from 'react';
import * as ReactDOMClient from 'react-dom/client';
import 'antd/dist/antd.css';
import 'reset-css';
import {App} from './components/App';
import {Provider} from "react-redux";
import {store} from "./store";

const container = document.createElement('div');
container.id = 'root';

const root = ReactDOMClient.createRoot(container);
document.body.appendChild(container);

root.render(
    <Provider store={store}>
        <App />
    </Provider>
);
