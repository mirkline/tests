import axios from "axios";
import {API_KEY_3, API_URL} from "../../utils/api";
import {movieSelector} from "./movieSelectors";

export const GET_MOVIES = 'GET_MOVIES'
export const IS_LOADING_MOVIES = 'IS_LOADING_MOVIES'
export const CURRENT_PAGE = 'CURRENT_PAGE'
export const SORT_BY = 'SORT_BY'
export const LOADING_MOVIES_ERROR = 'LOADING_MOVIES_ERROR'
export const ADD_MOVIES = 'ADD_MOVIES'
export const MOVIE_MODAL = 'MOVIE_MODAL'
export const DELETE_MOVIE = 'DELETE_MOVIE'


export const addMovies = (payload) => ({type: GET_MOVIES, payload})
export const updateMovies = (payload) => ({type: ADD_MOVIES, payload})
export const loadingMovies = (payload) => ({type: IS_LOADING_MOVIES, payload})
export const changePage = (payload) => ({type: CURRENT_PAGE, payload})
export const sortPrinciple = (payload) => ({type: SORT_BY, payload})
export const controlModal = (payload) => ({type: MOVIE_MODAL, payload})
export const deleteMovie = (id) => ({type: DELETE_MOVIE, payload: id})
export const errorMovies = (payload) => ({type: LOADING_MOVIES_ERROR, payload})


export const getMovies = () => {
    return async (dispatch, getState) => {
        const state = getState();
        const {currentPage, sortBy, isLoadingMovies}= movieSelector(state)
        if(isLoadingMovies) {
            return
        }
        try {
            dispatch(loadingMovies(true))
            const response = await axios.get(`${API_URL}/discover/movie?api_key=${API_KEY_3}&language=ru-RU&sort_by=${sortBy}&page=${currentPage}`)
            dispatch(loadingMovies(false))

            if(currentPage === 1){
                dispatch(addMovies(response.data.results))
            }else {
                dispatch(updateMovies(response.data.results))
            }
            dispatch(changePage(currentPage + 1))
        } catch (e) {
            dispatch(errorMovies('Произошла ошибка'))
        }
    }
}

