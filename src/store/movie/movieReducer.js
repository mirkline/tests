import {
    GET_MOVIES,
    IS_LOADING_MOVIES,
    LOADING_MOVIES_ERROR,
    CURRENT_PAGE,
    SORT_BY,
    ADD_MOVIES, MOVIE_MODAL, DELETE_MOVIE
} from "./movieAction";

export const initialState = {
    movies: [],
    isLoadingMovies: false,
    currentPage: 1,
    sortBy: 'popularity.desc',
    movieModal: {
        id: null,
        isModal: false
    },
    error: ''
}

export const movieReducer = (state = initialState, action) => {
    switch (action.type) {
        case IS_LOADING_MOVIES:
            return { ...state, isLoadingMovies: action.payload, error: null}
        case GET_MOVIES:
            return { ...state, movies: action.payload}
        case ADD_MOVIES:
            return { ...state, movies: [...state.movies, ...action.payload]}
        case CURRENT_PAGE:
            return { ...state, currentPage: action.payload}
        case SORT_BY:
            return { ...state, sortBy: action.payload, currentPage: 1}
        case MOVIE_MODAL:
            return { ...state, movieModal: {id: action.payload.id, isModal: action.payload.isModal}}
        case DELETE_MOVIE:
            return { ...state, movies: state.movies.filter((item) => item.id !== action.payload)}
        case LOADING_MOVIES_ERROR:
            return { ...state, movies: [], isLoadingMovies: false, error: action.payload}
        default:
            return state
    }
}