import {combineReducers} from "redux";
import {genreReducer} from "./genres/genreReducer";
import {movieReducer} from "./movie/movieReducer";


export const rootReducer = combineReducers({
    genres: genreReducer,
    movies: movieReducer
})