import {ADD_GENRES, IS_LOADING_GENRES, LOADING_GENRES_ERROR} from "./genreAction";

export const initialState = {
    genres: [],
    isLoadingGenres: true,
    error: ''
}

export const genreReducer = (state = initialState, action) => {
    switch (action.type) {
        case IS_LOADING_GENRES:
            return { ...state, isLoadingGenres: action.payload, error: null}
        case ADD_GENRES:
            return { ...state, genres: action.payload, isLoadingGenres: false, error: null}
        case LOADING_GENRES_ERROR:
            return { ...state, genres: [], isLoadingGenres: false, error: action.payload}
        default:
            return state
    }
}