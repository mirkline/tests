import axios from "axios";
import {API_KEY_3, API_URL} from "../../utils/api";


export const ADD_GENRES = 'GET_GENRES'
export const IS_LOADING_GENRES = 'IS_LOADING_GENRES'
export const LOADING_GENRES_ERROR = 'LOADING_GENRES_ERROR'

export const addGenres = (payload) => ({type: ADD_GENRES, payload})
export const loadingGenres = (payload) => ({type: IS_LOADING_GENRES, payload})
export const errorGenres = (payload) => ({type: LOADING_GENRES_ERROR, payload})

export const getGenres = () => {
    return async dispatch => {
        try {
            dispatch(loadingGenres(true))
            const response = await axios.get(`${API_URL}/genre/movie/list?api_key=${API_KEY_3}&language=ru`)
            dispatch(addGenres(response.data.genres))
        } catch (e) {
            dispatch(errorGenres('Произошла ошибка'))
        }
    }
}

