import {useSelector} from "react-redux";
import {movieSelector} from "../store/movie/movieSelectors";
import {useEffect, useState} from "react";
import axios from "axios";
import {API_KEY_3, API_URL} from "../utils/api";

export const useGetMovieModal = () => {
    const [movie, setMovie] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const {movieModal} = useSelector(movieSelector);

    useEffect(() => {
        if(movieModal.id === null){
            return
        }
        axios.get(`${API_URL}/movie/${movieModal.id}?api_key=${API_KEY_3}&language=ru-RU`)
            .then( response => {
                setMovie(response.data)
                setIsLoading(false)
            })
            .catch(error => {
                console.log(error);
            })
    }, [movieModal]);

    return {movie: movie, isLoading: isLoading}
}
