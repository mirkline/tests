import React from 'react';
import {Button, Image, Modal, Spin} from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {movieSelector} from "../../store/movie/movieSelectors";
import {controlModal} from "../../store/movie/movieAction";
import {useGetMovieModal} from "../../hooks/useGetMovieModal";
import {IMG_URL} from "../../utils/api";
import noPhoto from "../../utils/noPhoto.jpg";
import {createUseStyles} from "react-jss";

const useStyles = createUseStyles({
    spinModal: {
        position: 'absolute',
        marginLeft: '50%',
        marginTop: '1em',
    },
    modal: {
        "& h1": {
            textAlign: 'center',
            fontSize: '18px'
        },
        "& h2": {
            fontSize: '15px'
        },
    }
});

const MovieModal = () => {
    const { spinModal, modal } = useStyles();

    const dispatch = useDispatch()
    const {movieModal} = useSelector(movieSelector);

    const {movie, isLoading} = useGetMovieModal();

    const handleCancel = () => {
        dispatch(controlModal({
            id: null,
            isModal: false
        }))
    };
    return (
        <>
            {isLoading && <Spin size='large' className={spinModal}/>}
            <Modal
                title={
                    <>
                        <h1>{movie.tagline || movie.title}</h1>
                        <Image
                            // width={'75%'}
                            src={movie.poster_path ? `${IMG_URL}${ movie.poster_path }` : noPhoto }
                        />
                    </>
                }
                visible={movieModal.isModal}
                className={modal}
                onCancel={handleCancel}
                footer={[
                   <Button key="back" onClick={handleCancel} type="primary">
                       Закрыть
                   </Button>,
                ]}
            >
                <h2>Оригинальное название {movie.original_title}</h2>
                <p>{movie.overview}</p>
                <h2>Дата выхода {movie.release_date}</h2>
                <h2>Рейтинг {movie.vote_average}</h2>
                <h2>Количество голосов {movie.vote_count}</h2>
            </Modal>
        </>
    );
};

export {MovieModal};
