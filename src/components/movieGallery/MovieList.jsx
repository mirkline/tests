import React, { useEffect } from "react";
import { Layout, Row, Col, Spin } from 'antd';
import { createUseStyles } from 'react-jss';
import { MovieItem } from "./MovieItem";
import {useDispatch, useSelector} from "react-redux";
import {movieSelector} from "../../store/movie/movieSelectors";
import { getMovies } from "../../store/movie/movieAction";

const { Content } = Layout;

const useStyles = createUseStyles({
        spin: {
            margin: 'auto',

        },
        moviesCol: {
            padding: '0 0.1em'
        }
    }
);

const MovieList = () => {
    const dispatch = useDispatch()
    const { spin, moviesCol } = useStyles();

    const {movies, isLoadingMovies} = useSelector(movieSelector);

    useEffect(() => {
        document.addEventListener('scroll', scrollMovies)
        return () => {
            document.removeEventListener('scroll', scrollMovies)
        };
    }, [])

    const scrollMovies = (e) => {
        if(e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 20) {
            dispatch(getMovies())
        }
    }

    return (
        <Layout
            style={{
                margin: '0',
                padding: '0',
            }}>
            <Content >
                <Row>
                    {movies.map(movie => (
                        <Col sm={24} md={12} lg={12} xl={8} xxl={6} className={moviesCol} key={movie.id}>
                            <MovieItem key={movie.id} data={movie} />
                        </Col>
                    ))}
                </Row>
                {isLoadingMovies && <Spin size='large' className={spin}/> }
            </Content>
        </Layout>
    );
};

export {MovieList};