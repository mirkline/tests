import React, {useCallback} from "react";
import { Image, Card, Button } from 'antd';
import { createUseStyles } from "react-jss";
import {useDispatch} from "react-redux";
import {controlModal, deleteMovie} from "../../store/movie/movieAction";
import {IMG_URL} from "../../utils/api";
import noPhoto from "../../utils/noPhoto.jpg";

const { Meta } = Card;

const useStyles = createUseStyles({
    card: {
        textAlign: 'center',
        paddingTop: '0.5rem'
    }
});

const MovieItem = ({data}) => {
    const { card } = useStyles();

    const dispatch = useDispatch()

    const openModal = () => {
        dispatch(controlModal({
            id: data.id,
            isModal: true
        }))
    }

    const onDelete = useCallback(() => {
        dispatch(deleteMovie(data.id))
    },[data])

    return (
        <Card
            className={card}
            cover={
                <Image
                    width={'75%'}
                    src={data.poster_path ? `${IMG_URL}${ data.poster_path }` : noPhoto }
                />
            }
            actions={[
                <Button type="primary" onClick = {openModal}>Подробнее</Button>,
                <Button type="primary" danger
                        onClick={onDelete}>
                    Удалить
                </Button>
            ]}
            hoverable
        >
            <Meta
                title={data.title}
                description={`Рейтинг ${data.vote_average}  Дата выхода ${data.release_date}`}
            />
        </Card>
    );
};

export {MovieItem};