import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import { Layout } from 'antd';
import { MovieList } from './MovieList';
import { MovieTabs } from "./MovieTabs";
import {getMovies} from "../../store/movie/movieAction";
import {MovieModal} from "./MovieModal";
import {movieSelector} from "../../store/movie/movieSelectors";

const MovieContainer = () => {
    const dispatch = useDispatch()

    const {sortBy} = useSelector(movieSelector);
    const {movieModal} = useSelector(movieSelector);

    useEffect(() => {
        dispatch(getMovies())
    },[sortBy])

    return (
        <Layout>
            <MovieTabs />
            <MovieList />
            {movieModal.isModal && <MovieModal/>}
        </Layout>
    );
};

export {MovieContainer};