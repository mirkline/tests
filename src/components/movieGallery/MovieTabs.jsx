import React, {useEffect, useMemo} from "react";
import {Layout, Menu, Spin} from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {getGenres} from "../../store/genres/genreAction";
import {genreSelector} from "../../store/genres/genreSelectors";
import {addMovies, sortPrinciple} from "../../store/movie/movieAction";
import {movieSelector} from "../../store/movie/movieSelectors";

const { Sider } = Layout;

const menuItems = [
    {key: 'popularity.desc', label: 'По популярности'},
    {key: 'release_date.desc', label: 'Дата выхода'},
    {key: 'vote_count.desc', label: 'Кол-во голосов'},
    {key: 'vote_average.desc', label: 'По рейтингу'},
    {key: 'revenue.desc', label: 'По Доходности'},
    // {key: 'primary_release_date.desc', label: 'Основная дата выхода'},
];

const MovieTabs = ( ) => {
    const dispatch = useDispatch()

    const {genres, isLoadingGenres} = useSelector(genreSelector);
    const {sortBy, movies} = useSelector(movieSelector)
    useEffect(() => {
        dispatch(getGenres())
    },[])

    const newMenuItems = useMemo(() =>
            [
                ...menuItems,
                {
                    key: 'genre',
                    label: 'Жанры',
                    children: genres.map(element => ({
                        key: element.id, label: element.name
                    }))
                }
            ]
        ,[genres]);

    const filterByGenre = (value) => {
        const updateMovies = movies.filter(
            (item) => item.genre_ids.some((e) => e === Number(value))
        );
        dispatch(addMovies(updateMovies));
    }

    const updateSortBy = (value, keyPath) => {
        if(value !== sortBy && keyPath === undefined){
            dispatch(sortPrinciple(value))
        }
        if(keyPath){
            filterByGenre(value);
        }
    };

    if(isLoadingGenres){
        return <Spin size='large'/>
    }
    return (
        <Sider
            width={200}
            style={{marginRight: '0.25rem'}}
            collapsedWidth={40}
            collapsible={true}
            defaultCollapsed={false}>
            <Menu
                theme='dark'
                mode="inline"
                defaultSelectedKeys={sortBy}
                // defaultOpenKeys={['sub1']}
                onClick={(event) => updateSortBy(event.key, event.keyPath[1])}
                style={{
                    paddingTop: '0.5em',
                    height: '100%',
                    borderRight: 0,
                }}
                items={newMenuItems}
            />
        </Sider>
    );
};

export {MovieTabs};