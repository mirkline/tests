import React from 'react';
import {Routes, Route, BrowserRouter} from 'react-router-dom';
import { Layout } from 'antd';
// Components
import  Header  from "./header/Header";
import { MovieContainer } from "./movieGallery/MovieContainer";

const App = () => {

  return (
    <BrowserRouter>
      <Layout className='layout'>
        <Header />
        <Routes>
            <Route path='/' element={<MovieContainer />}/>
            <Route path='/todoList'/>
        </Routes>
      </Layout>
    </BrowserRouter>
  );
};

export {App};
