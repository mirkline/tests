import React from 'react';
import { createUseStyles } from 'react-jss';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';

const useStyles = createUseStyles({
  menu: {
    lineHeight: '64px'
  },
  menuItem: {
    marginLeft: '1em',
  }
});

const { Header } = Layout;

const Head = () => {
  const { menu, menuItem } = useStyles();
  
  return (
    <Header>
      <Menu theme='dark' mode='horizontal' className={menu}>
        <Menu.Item key='movieGallery' className={menuItem}>
          <Link to= '/'>MovieGallery</Link>
        </Menu.Item>
        <Menu.Item key='todoList' className={menuItem}>
          <Link to= '/todoList'>TodoList</Link>
        </Menu.Item>
      </Menu>
    </Header>
  );
};

export default Head;